A salon garnished with contemporary touches and the experience of old world couture, designed to create a unique experience for today’s bride. Ever After is a pioneer in the bridal industry.

Address: 2977 McFarlane Rd, Suite 100B, Miami, FL 33133, USA

Phone: 305-444-7300
